#include <cstdlib>
#include <cassert>

#include <string>
#include <iostream>
#include <vector>

// Number of steps to take for 1mm travel.
constexpr int steps_per_mm_X = 10;
constexpr int steps_per_mm_Y = 10;
constexpr int steps_per_mm_Z = 4000;

using ValueType = double;

// TODO: Convert to iterator version, which accepts std::string 
std::vector<std::string> split(const char *str, char c = ' ')
{
    std::vector<std::string> result;
    do
    {
        const char *begin = str;

        while(*str != c && *str)
            str++;

        result.push_back(std::string(begin, str));
    } while (0 != *str++);

    return result;
}

template<typename T>
class Parser
{
    public:
        static T parseVal(std::string str);
};

template<>
class Parser<long>
{
    public:
        static long parseVal(std::string str)
        {
            return std::stoi(str);
        }
};

template<>
class Parser<double>
{
    public:
        static double parseVal(std::string str)
        {
            return std::stod(str);
        }
};

template<typename T>
T parseVal(std::string str)
{
    return Parser<T>::parseVal(str);
}


int main()
{
    // Read STDIN line by line
    std::string line;

    // Process line by line
    while(std::getline(std::cin, line))
    {
        std::vector<std::string> parts = split(line.c_str());

        // Validate the input
        assert(parts.size() == 4);
        // Pull out the parts
        std::string strG = parts[0];
        std::string strX = parts[1];
        std::string strY = parts[2];
        std::string strZ = parts[3];
        // Validate each of them
        assert(strG == "G0");
        assert(strX[0] == 'X');
        assert(strY[0] == 'Y');
        assert(strZ[0] == 'Z');

        // Erase the identifying letters
        strX.erase(strX.begin());
        strY.erase(strY.begin());
        strZ.erase(strZ.begin());
        
        // Parse out the values
        ValueType posX = parseVal<ValueType>(strX);
        ValueType posY = parseVal<ValueType>(strY);
        ValueType posZ = parseVal<ValueType>(strZ);
/*
        std::cout << posX << std::endl;
        std::cout << posY << std::endl;
        std::cout << posZ << std::endl;
*/

        ValueType scaled_posX = posX / steps_per_mm_X;
        ValueType scaled_posY = posY / steps_per_mm_Y;
        ValueType scaled_posZ = posZ / steps_per_mm_Z;

        // Output as human readable
        //std::cout << "X: " << posX << "\tY: " << posY << "\tZ: " << posZ << std::endl;
        // Output as a GCODE
        std::cout << "G0 X" << scaled_posX << " Y" << scaled_posY << " Z" << scaled_posZ << std::endl;
    }
    return EXIT_SUCCESS;
}
