all:
	g++ -std=c++11 main.cpp -o main

test: all
	bash -c 'echo -e "G0 X1 Y1 Z0" | ./main'
